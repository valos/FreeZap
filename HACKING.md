Building
===

FirefoxOS
---

Build the package:

    make dist-firefox-os

Ubuntu-Touch
---

Build the package:

    make dist-ubuntu-touch

Upload to device (for test):

    make install-ubuntu-touch

Note: the device should be in developer mode.
